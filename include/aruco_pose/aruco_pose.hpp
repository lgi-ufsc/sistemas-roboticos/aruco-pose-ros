/************************************************
 * Author: Lucas de Camargo Souza @lcs
 *         lucas_camargo@hotmail.com.br
 * 
 * Modifications:
 * 09/08/21 @lcs Documented class
 ***********************************************/

#ifndef __ARUCO_POSE_h__
#define __ARUCO_POSE_h__

#include <aruco.h>
#include <opencv2/opencv.hpp>

#include <vector>
#include <map>

namespace aruco_pose
{
    using namespace aruco;

    /**
     * Wrapper for detecting markers using the ArUco library
     * 
     * It uses the required ArUco objects and methods to detect
     * and calculate the pose of fiducial markers. It also implements
     * a method for calculating an objects pose based on three
     * ArUco markers points.
     * 
     */
    class ArucoPose
    {
    public:
        struct Pose
        {
            cv::Mat pos;
            cv::Mat rot;
        };

        /**
         * ArucoPose Constructor. 
         * 
         * It corresponds to a set of markers with the same size and same dictionary.
         * 
         * @param marker_size Size of markers in meters [m].
         * @param dictionary Dictionary type of markers.
         * @param err_correction Error correction rate [0,1].
         */
        ArucoPose(float marker_size, Dictionary::DICT_TYPES dictionary = Dictionary::ARUCO, float err_correction = (0.0F));
        /**
         * This constructor uses a list of markers that shall be processed. 
         * It requires 3 markers when Three Points Estimation Calculation 
         * is used. The list order matters.
         * 
         * @param marker_size Size of markers in meters [m].
         * @param marker_list List of markers IDs to be proessed. It is required when working with Three Points Estimation. The list order matters.
         * @param dictionary Dictionary type of markers.
         * @param err_correction Error correction rate [0,1].
         */ 
        ArucoPose(float marker_size, std::vector<int> marker_list, Dictionary::DICT_TYPES dictionary = Dictionary::ARUCO, float err_correction = (0.0F));

        /**
         * Camera parameters are required for pose calculation. If no parameters are set, they are 
         * assumed to be unit values.
         * 
         * @param cameraMatrix 3x3 Matrix corresponding to the camera matrix parameters. 
         * @param cameraDistortion 5 elements array corresponding to the camera distortion.
         * @param cameraSize Image size in pixels.
         * 
         * @see https://docs.opencv.org/4.5.2/dc/dbb/tutorial_py_calibration.html
         */ 
        void setCameraParameters(const cv::Mat cameraMatrix, const cv::Mat cameraDistortion, const cv::Size cameraSize = cv::Size());
        /**
         * Camera parameters are required for pose calculation. If no parameters are set, they are 
         * assumed to be unit values.
         * 
         * @param camParameters ArUco data type for camera parameters.
         */ 
        void setCameraParameters(const CameraParameters camParameters);
        CameraParameters getCameraParameters(void) { return camParams; }

        /**
         * Markers dictionary. If no value is set, it is assumed to be the ARUCO disctionary.
         * 
         * @param dictionary Dictionary type of markers.
         * @param err_correction Error correction rate [0,1].
         */ 
        void setDictionary(Dictionary::DICT_TYPES dictionary, float err_correction = (0.0F));

        /**
         * \brief   The DetectionMode enum defines the different possibilities for detection.
        *           Specifies the detection mode. We have preset three types of detection modes. These are
        *           ways to configure the internal parameters for the most typical situations. The modes are:
        *           - DM_NORMAL: In this mode, the full resolution image is employed for detection and slow threshold method. Use this method when
        *           you process individual images that are not part of a video sequence and you are not interested in speed.
        *           
        *           - DM_FAST: In this mode, there are two main improvements. First, image is threshold using a faster method using a global threshold.
        *           Also, the full resolution image is employed for detection, but, you could speed up detection even more by indicating a minimum size of the
        *           markers you will accept. This is set by the variable minMarkerSize which shoud be in range [0,1]. When it is 0, means that you do not set
        *           a limit in the size of the accepted markers. However, if you set 0.1, it means that markers smaller than 10% of the total image area, will not
        *           be detected. Then, the detection can be accelated up to orders of magnitude compared to the normal mode.
        *           
        *           - DM_VIDEO_FAST: This is similar to DM_FAST, but specially adapted to video processing. In that case, we assume that the observed markers
        *           when you call to detect() have a size similar to the ones observed in the previous frame. Then, the processing can be speeded up by employing smaller versions
        *           of the image automatically calculated.
        *
        */
        void setDetectionMode(const aruco::DetectionMode dm) { mdetector.setDetectionMode(dm); }

        /**
         * Process an image frame and execute the detection of the pose of markers. 
         * If a marker list is set, it only processes those in the list.
         * 
         * @param image Image to be processed.
         * @param draw_markers Whether to draw the 3D pose of the markers on the image passed.
         * @return A mapping array to the processed markers, mapped by their ID.
         */ 
        std::map<int, Marker> run(cv::Mat &image, bool draw_markers = false);
        /*
         * Thought for stereo cameras. NOT IMPLEMENTED.
         */
        std::map<int, Marker> run(cv::Mat &left_image, cv::Mat &right_image, bool draw_markers = false);

        /**
         * Calculates the resulting orientation of the position of three markers, set by marker_list. 
         * 
         * @param markers Calculated markers resulted from run() call. There must be three markers with valid pose.
         * @param output[out] Calculated pose.
         * @return True if success, false if calculation failed.
         */ 
        bool threePointsEstimation(std::map<int, Marker> markers, Pose &output);
        /**
         * Calculates the resulting orientation of the position of three markers, set by marker_list. 
         * Draws the 3D resulting pose on the image.
         * 
         * @param markers Calculated markers resulted from run() call. There must be three markers with valid pose.
         * @param output[out] Calculated pose.
         * @param image_output Image to draw the 3D estimated pose.
         * @return True if success, false if calculation failed.
         */ 
        bool threePointsEstimation(std::map<int, Marker> markers, Pose &output, cv::Mat &image_output);

        std::vector<int> marker_list(void) { return mlist; }

        void drawMarker(cv::Mat &frame, Marker marker);
        void drawMarker(cv::Mat &frame, std::map<int, Marker> markers);

        void yPerpendicular(bool value) { yperpendicular = value; }

        float getMarkerSize() { return msize; }

    protected:
        void _construct(void);

        std::vector<int> mlist; // marker_list
        cv::Size camSize;
        cv::Mat camMatrix, camDist;
        MarkerDetector mdetector;
        std::map<uint32_t, aruco::MarkerPoseTracker> tracker;
        CameraParameters camParams;

    private:
        float msize;
        Dictionary::DICT_TYPES dict;
        bool camParams_set = false;
        float err;
        bool yperpendicular = false;
    };
}

#endif
