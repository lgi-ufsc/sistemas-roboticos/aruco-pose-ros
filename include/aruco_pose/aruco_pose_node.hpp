#ifndef __ARUCO_POSE_NODE_h__
#define __ARUCO_POSE_NODE_h__

#include <ros/ros.h>
#include <aruco_pose/aruco_pose.hpp>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <std_msgs/UInt32MultiArray.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <cv_bridge/cv_bridge.h>

#include <sstream>
#include <exception>
#include <typeinfo> 
#include <vector>

namespace
{
    const int32_t PUBLISHER_BUFFER_SIZE = 20;
    const int32_t OUT_IMAGE_BUFFER_SIZE = 1;
}

namespace aruco_pose
{
    class ArucoPoseNode
    {
    public:
        ArucoPoseNode(ros::NodeHandle *nh_);
        
        void init(void);

    protected:
        void imageCallback(const sensor_msgs::ImageConstPtr &image);

        std::unique_ptr<ArucoPose> arp;
        ros::Publisher pubMsg;
        ros::Publisher pubMsgArray;
        ros::Publisher pubImage;
        ros::Publisher pub3PE;
        ros::Publisher pub3PEArray;
        ros::NodeHandle *nh;
        ros::Subscriber sub;

        std::string image_topic;
        bool three_points_estimation = false;
        bool draw_markers = true;
        int averaging = 1;

        bool marker_base = false;
        std::string frame_id = "base_link";
        float scale;
        float width;
        float alpha;

        std::vector<std::map<int, aruco::Marker>> marker_queue;

    private:
        std::map<int, aruco::Marker> __marker_averaging(std::vector<std::map<int, aruco::Marker>> markers);        
        std::vector<std::string> __str_split(const std::string &s, const std::string &d);
        CameraParameters __cameraInfoParser(const sensor_msgs::CameraInfo &cam_info, bool use_rectified_params=false);
        visualization_msgs::MarkerArray __get_marker_base(const visualization_msgs::Marker m);
        visualization_msgs::Marker __marker_msg(std::string ns, int32_t id, cv::Mat Tvec, cv::Mat Rvec);
    };
}

#endif
