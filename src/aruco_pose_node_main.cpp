#include <aruco_pose/aruco_pose_node.hpp>
#include <ros/ros.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "aruco_pose_node");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(0);
    spinner.start();

    ROS_INFO("Starting aruco_pose_node...");
    aruco_pose::ArucoPoseNode poseNode(&nh);
    poseNode.init();

    ros::waitForShutdown();

    return 0;
}