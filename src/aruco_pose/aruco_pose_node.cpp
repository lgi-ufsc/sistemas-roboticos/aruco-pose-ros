#include <aruco_pose/aruco_pose_node.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

using namespace aruco_pose;

std::map<std::string, aruco::DetectionMode> DETECTION_MODE = 
{
    {"DM_NORMAL",       aruco::DetectionMode::DM_NORMAL},
    {"DM_FAST",         aruco::DetectionMode::DM_FAST},
    {"DM_VIDEO_FAST",   aruco::DetectionMode::DM_VIDEO_FAST}
};

ArucoPoseNode::ArucoPoseNode(ros::NodeHandle *nh_)
    : nh(nh_)
{
    /* ---- Marker ---- */
    ROS_INFO("Setting up Markers");
    float marker_size = 0.1;
    std::string dictionary = "ARUCO";
    std::string marker_list_str;
    std::vector<int> marker_list;

    ros::param::get("~marker_size", marker_size);
    ros::param::get("~dictionary", dictionary);
    ros::param::get("~marker_list", marker_list_str);
    ros::param::get("~draw_markers", draw_markers);
    
    ros::param::get("~marker_base", marker_base);
    ros::param::get("~frame_id", frame_id);
    ros::param::get("~scale", scale);
    ros::param::get("~width", width);
    ros::param::get("~alpha", alpha);

    if(!marker_list_str.empty())
    {
        std::vector<std::string> parsed = __str_split(marker_list_str, ",");
        for(auto &s : parsed) marker_list.push_back(stod(s));
    }

    
    /* ---- Aruco Config ---- */
    ROS_INFO("Setting up Aruco");
    float err_correction = (0.0F);
    bool yperpendicular = false;
    std::string detection_mode = "DM_NORMAL";

    ros::param::get("~three_poins_estimation", three_points_estimation);
    ros::param::get("~err_correction", err_correction);
    ros::param::get("~averaging", averaging);
    ros::param::get("~yperpendicular", yperpendicular);
    ros::param::get("~detection_mode", detection_mode);
    
    if(three_points_estimation)
    {
        if(marker_list.size() != 3) throw std::logic_error("3 Point Estimation needs 3 Marker IDs");
    //     arp.reset(new ThreePointsEstimation(marker_size, marker_list, Dictionary::getTypeFromString(dictionary), err_correction));
    }
    // else
    arp.reset(new ArucoPose(marker_size, marker_list,Dictionary::getTypeFromString(dictionary), err_correction));
    arp->yPerpendicular(yperpendicular);

    auto mlist = arp->marker_list();
    if(!mlist.empty())
    {            
        std::string mlist_str = "Marker list: ";
        for(auto &i : mlist)
            mlist_str += std::to_string(i) + "; ";
        ROS_INFO("%s", mlist_str.c_str());
    }

    if(DETECTION_MODE.find(detection_mode) == DETECTION_MODE.end())
    {
        ROS_WARN("Detection mode %s invalid. Using standard DM_NORMAL. Possible values are: DM_NORMAL, DM_FAST, DM_VIDEO_FAST", detection_mode.c_str());
        detection_mode = "DM_NORMAL";
    }
    arp->setDetectionMode(DETECTION_MODE[detection_mode]);

    /* ---- Camera Parameters ---- */
    ROS_INFO("Setting up camera parameters");
    std::string info_topic = "/";
    bool use_camera_info = false;
    bool use_rectified_parameters = false;

    ros::param::get("~image_topic", image_topic);
    ros::param::get("~use_camera_info", use_camera_info);
    ros::param::get("~use_rectified_parameters", use_rectified_parameters);

    if(image_topic.empty())
        throw std::logic_error("image_topic parameter must be set");
    
    if(use_camera_info)
    {
        std::vector<std::string> parsed = __str_split(image_topic, "/");

        for(int i = 0; i < parsed.size()-1; i++)
            info_topic += parsed[i] + "/";
        info_topic += "camera_info";

        ROS_INFO("Waiting for camera info message on topic '%s'... ", info_topic.c_str());
        sensor_msgs::CameraInfoConstPtr info = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(info_topic, *nh);
        ROS_INFO("Camera info message received!");

        ROS_INFO("Setting up Camera Parameters...");
        CameraParameters camParams = __cameraInfoParser(*info, use_rectified_parameters);
        arp->setCameraParameters(camParams);
        ROS_INFO("Camera Parameters set");
    }

    /* Node Parameters */
    ROS_INFO("Setting Up Nodes");
    std::string output_image_topic = "aruco_pose/image";
    std::string three_points_estimation_topic = "aruco_pose/3points_estimation";
    std::string markers_topic = "aruco_pose/markers";
    ros::param::get("~markers_topic", markers_topic);
    ros::param::get("~output_image_topic", output_image_topic);
    ros::param::get("~three_points_estimation_topic", three_points_estimation_topic);
    pubMsg = nh->advertise<visualization_msgs::Marker>(markers_topic, PUBLISHER_BUFFER_SIZE);
    pubImage = nh->advertise<sensor_msgs::Image>(output_image_topic, OUT_IMAGE_BUFFER_SIZE);
    pub3PE = nh->advertise<visualization_msgs::Marker>(three_points_estimation_topic, PUBLISHER_BUFFER_SIZE);

    if(marker_base)
    {
        pubMsgArray = nh->advertise<visualization_msgs::MarkerArray>(markers_topic+"_array",PUBLISHER_BUFFER_SIZE);
        pub3PEArray = nh->advertise<visualization_msgs::MarkerArray>(three_points_estimation_topic+"_array", PUBLISHER_BUFFER_SIZE);
    }
}

void ArucoPoseNode::init(void)
{
    ROS_INFO("Subscribing to %s", image_topic.c_str());
    sub = nh->subscribe(image_topic, 1, &aruco_pose::ArucoPoseNode::imageCallback, this);
    ROS_INFO("Done.");
}

void ArucoPoseNode::imageCallback(const sensor_msgs::ImageConstPtr &image)
{
    bool publishMsg = pubMsg.getNumSubscribers() > 0;
    bool publishImage = pubImage.getNumSubscribers() > 0;
    bool publish3PE = pub3PE.getNumSubscribers() > 0;

    if((!publishMsg && !publishImage) && !publish3PE)
    {
        marker_queue.clear();
        return;
    }

    cv_bridge::CvImagePtr cv_ptr;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(image);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    frame_id = image->header.frame_id;
    auto m = arp->run(cv_ptr->image, false);
    if(!m.empty())
        marker_queue.push_back(m);

    if(marker_queue.size() >= averaging)
    {
        auto markers = __marker_averaging(marker_queue);
        marker_queue.clear();

        if(draw_markers)
            arp->drawMarker(cv_ptr->image, markers);

        if(publishMsg)
        {
            for(auto &m : markers)
            {
                visualization_msgs::Marker out = __marker_msg("markers", m.first, m.second.Tvec, m.second.Rvec);

                if(marker_base)
                    pubMsgArray.publish(__get_marker_base(out));
                
                pubMsg.publish(out);
            }
        }

        if(three_points_estimation && !markers.empty())
        {
            ArucoPose::Pose p;
            bool est;
            try
            {
                est = arp->threePointsEstimation(markers, p, cv_ptr->image);
            }
            catch(const std::exception& e)
            {
                std::cerr << "Three points estimation FAILED: " << e.what() << '\n';
            }
            
            if(publish3PE && est)
            {
                try
                {
                    visualization_msgs::Marker out = __marker_msg("3points_estimation", 3, p.pos, p.rot);

                    if(marker_base)
                        pub3PEArray.publish(__get_marker_base(out));

                    pub3PE.publish(out);
                }
                catch(const std::exception& e)
                {
                    std::cerr << "Three points estimation output FAILED: " << e.what() << '\n';
                }
            }
        }
    }

    if(publishImage)
        pubImage.publish(cv_ptr->toImageMsg());
}

std::map<int, aruco::Marker> ArucoPoseNode::__marker_averaging(std::vector<std::map<int, aruco::Marker>> markers)
{
    std::map<int, aruco::Marker> ret;
    std::map<int, int> count;

    for(auto &v : markers)
    {
        for(auto &m : v)
        {
            auto elem = ret.find(m.first);

            if(elem == ret.end())
            {
                ret[m.first] = m.second;
                count[m.first] = 1;
            }
            else
            {
                elem->second.Tvec += m.second.Tvec;
                elem->second.Rvec += m.second.Rvec;
                count[m.first] += 1;
            }
        }
    }

    for(auto &m : ret)
    {
        m.second.Tvec /= count[m.first];
        m.second.Rvec /= count[m.first];
    }

    return ret;
}

std::vector<std::string> ArucoPoseNode::__str_split(const std::string &s, const std::string &d)
{
    std::vector<std::string> ret;
    auto start = 0U;
    auto end = s.find(d);
    while(end != std::string::npos)
    {
        std::string tok = s.substr(start, end-start);
        start = end+1;
        end = s.find(d, start);

        ret.push_back(tok);
    }
    ret.push_back(s.substr(start,end));

    return ret;
}

CameraParameters ArucoPoseNode::__cameraInfoParser(const sensor_msgs::CameraInfo &cam_info, bool use_rectified_params)
{
    cv::Mat cameraMatrix(3, 3, CV_64FC1);
    cv::Mat distorsionCoeff(4, 1, CV_64FC1);
    cv::Size size(cam_info.width, cam_info.height);

    if (use_rectified_params)
    {
        cameraMatrix.setTo(0);
        cameraMatrix.at<double>(0, 0) = cam_info.P[0];
        cameraMatrix.at<double>(0, 1) = cam_info.P[1];
        cameraMatrix.at<double>(0, 2) = cam_info.P[2];
        cameraMatrix.at<double>(0, 3) = cam_info.P[3];
        cameraMatrix.at<double>(1, 0) = cam_info.P[4];
        cameraMatrix.at<double>(1, 1) = cam_info.P[5];
        cameraMatrix.at<double>(1, 2) = cam_info.P[6];
        cameraMatrix.at<double>(1, 3) = cam_info.P[7];
        cameraMatrix.at<double>(2, 0) = cam_info.P[8];
        cameraMatrix.at<double>(2, 1) = cam_info.P[9];
        cameraMatrix.at<double>(2, 2) = cam_info.P[10];
        cameraMatrix.at<double>(2, 3) = cam_info.P[11];

        for (int i = 0; i < 4; ++i)
        distorsionCoeff.at<double>(i, 0) = 0;
    }
    else
    {
        for (int i = 0; i < 9; ++i)
            cameraMatrix.at<double>(i % 3, i - (i % 3) * 3) = cam_info.K[i];

        if (cam_info.D.size() >= 4)
        {
            for (int i = 0; i < 4; ++i)
                distorsionCoeff.at<double>(i, 0) = cam_info.D[i];
        }
        else
        {
            ROS_WARN("Length of camera_info D vector is not 4, assuming zero distortion...");
            for (int i = 0; i < 4; ++i)
                distorsionCoeff.at<double>(i, 0) = 0;
        }
    }

    return CameraParameters(cameraMatrix, distorsionCoeff, size);
}

const double R2 = 0.70710678118;
visualization_msgs::MarkerArray ArucoPoseNode::__get_marker_base(const visualization_msgs::Marker m)
{
    visualization_msgs::MarkerArray out;

    tf2::Quaternion q;
    tf2::convert(m.pose.orientation , q);

    std::vector<tf2::Quaternion> b
    {
        {tf2::Quaternion(1,0,0,0)},
        {tf2::Quaternion(R2,R2,0,0)},
        {tf2::Quaternion(R2,0,R2,0)}
    };

    int count = 0;
    for(auto &i : b)
    {
        i = q*i;
        i.normalize();

        visualization_msgs::Marker msg;
        msg.id = m.id * 100 + count;
        msg.ns = m.ns + "_base";
        msg.header.frame_id = m.header.frame_id;
        msg.header.stamp = m.header.stamp;
        msg.type = visualization_msgs::Marker::ARROW;
        msg.action = visualization_msgs::Marker::ADD;
        msg.pose.position.x = m.pose.position.x;
        msg.pose.position.y = m.pose.position.y;
        msg.pose.position.z = m.pose.position.z;
        msg.pose.orientation.w = i.getW();
        msg.pose.orientation.x = i.getX();
        msg.pose.orientation.y = i.getY();
        msg.pose.orientation.z = i.getZ();
        msg.scale.x = m.scale.x;
        msg.scale.y = m.scale.y;
        msg.scale.z = m.scale.z;
        msg.color.a = m.color.a;
        msg.color.r = msg.color.g = msg.color.b = 0;

        if(count==0) msg.color.r = 1.0;
        else if (count==1) msg.color.g = 1.0;
        else msg.color.b = 1.0;

        out.markers.push_back(msg);
        
        count++;
    }


    return out;
}

visualization_msgs::Marker ArucoPoseNode::__marker_msg(std::string ns, int32_t id, cv::Mat Tvec, cv::Mat Rvec)
{
    visualization_msgs::Marker out;
    out.ns = ns;
    out.id = id;
    out.header.frame_id = frame_id;
    out.header.stamp = ros::Time::now();
    out.type = visualization_msgs::Marker::ARROW;
    out.action = visualization_msgs::Marker::ADD;
    out.pose.position.x = Tvec.at<float>(0);
    out.pose.position.y = Tvec.at<float>(1);
    out.pose.position.z = Tvec.at<float>(2);

    tf2::Quaternion q;
    q.setRPY(
        Rvec.at<float>(0),
        Rvec.at<float>(1),
        Rvec.at<float>(2)
    );

    q.normalize();
    
    tf2::convert(q, out.pose.orientation);

    out.scale.x = arp->getMarkerSize() * scale;
    out.scale.y = out.scale.z = arp->getMarkerSize() * width;
    out.color.a = alpha;
    out.color.r = 1.0;
    out.color.g = 0.0;
    out.color.b = 0.0;

    return out;
}

