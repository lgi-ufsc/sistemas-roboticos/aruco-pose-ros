#include <aruco_pose/aruco_pose.hpp>
#include <exception>
#include <iostream>

using namespace aruco_pose;

// ---> Aux. Function
void draw_axis(cv::Mat& Image, const cv::Mat& cam_matrix, const cv::Mat& dist_coef, const cv::Mat& Rvec, const cv::Mat& Tvec, float axis_size, int line_size)
{
    cv::Mat objectPoints(4, 3, CV_32FC1);
    objectPoints.at<float>(0, 0) = 0;
    objectPoints.at<float>(0, 1) = 0;
    objectPoints.at<float>(0, 2) = 0;
    objectPoints.at<float>(1, 0) = axis_size;
    objectPoints.at<float>(1, 1) = 0;
    objectPoints.at<float>(1, 2) = 0;
    objectPoints.at<float>(2, 0) = 0;
    objectPoints.at<float>(2, 1) = axis_size;
    objectPoints.at<float>(2, 2) = 0;
    objectPoints.at<float>(3, 0) = 0;
    objectPoints.at<float>(3, 1) = 0;
    objectPoints.at<float>(3, 2) = axis_size;

    std::vector<cv::Point2f> imagePoints;
    cv::projectPoints(objectPoints, Rvec, Tvec, 
        // static_cast<cv::Mat>( (cv::Mat_<float>(3,3) << 1,0,0, 0,1,0, 0,0,1 ) ), 
        cam_matrix,
        dist_coef, 
        // static_cast<cv::Mat>( (cv::Mat_<float>(4,1) << 0,0,0,0) ),
        imagePoints);

    // draw lines of different colours
    cv::line(Image, imagePoints[0], imagePoints[1], cv::Scalar(0, 0, 255, 255), line_size);
    cv::line(Image, imagePoints[0], imagePoints[2], cv::Scalar(0, 255, 0, 255), line_size);
    cv::line(Image, imagePoints[0], imagePoints[3], cv::Scalar(255, 0, 0, 255), line_size);
    putText(Image, "x", imagePoints[1], cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 255, 255), line_size);
    putText(Image, "y", imagePoints[2], cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 255, 0, 255), line_size);
    putText(Image, "z", imagePoints[3], cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(255, 0, 0, 255), line_size);
}
// <---


ArucoPose::ArucoPose(float marker_size, Dictionary::DICT_TYPES dictionary, float err_correction)
    : msize(marker_size), dict(dictionary), err(err_correction)
{
    _construct();
}

ArucoPose::ArucoPose(float marker_size, std::vector<int> marker_list, Dictionary::DICT_TYPES dictionary, float err_correction)
    : msize(marker_size), mlist(marker_list), dict(dictionary)
{
    _construct();
}

void ArucoPose::_construct(void)
{
    mdetector.setDictionary(dict, err);
}

void ArucoPose::setCameraParameters(const cv::Mat cameraMatrix, const cv::Mat cameraDistortion, const cv::Size cameraSize)
{
    camParams_set = false;
    camMatrix = cameraMatrix;
    camDist = cameraDistortion;
    camSize = cameraSize;
}

void ArucoPose::setCameraParameters(const CameraParameters camParameters)
{
    camParams = camParameters;
    camParams_set = true;
}

void ArucoPose::setDictionary(Dictionary::DICT_TYPES dictionary, float err_correction)
{
    mdetector.setDictionary(dictionary, err_correction);
}

std::map<int, Marker> ArucoPose::run(cv::Mat &image, bool draw_markers)
{
    if(!camParams_set)
    {
        if(camMatrix.empty())
            camMatrix = (cv::Mat_<double>(3,3) << 1,0,0, 0,1,0, 0,0,1 );
        if(camDist.empty())
            camDist = (cv::Mat_<int>(4,1) << 0,0,0,0);
        if(camSize.empty())
            camSize = cv::Size(image.cols, image.rows);

        camParams = CameraParameters(camMatrix, camDist, camSize);
        camParams_set = true;
    }

    std::vector<aruco::Marker> markers = mdetector.detect(image, camParams, msize, yperpendicular);
    std::map<int, Marker> ret;

    auto estimatePose = [&](Marker &m) -> void
    {
        tracker[m.id].estimatePose(m, camParams, msize);
        if(m.isPoseValid())
        {
            // Por algum motivo a biblioteca ArUco muda o tipo das matrizes às vezes!
            if(m.Tvec.cols == 1) cv::transpose(m.Tvec, m.Tvec);
            if(m.Rvec.cols == 1) cv::transpose(m.Rvec, m.Rvec);

            ret[m.id] = m;
            if(draw_markers && !m.Tvec.empty())
                drawMarker(image, m);
        }
    };
    
    for(auto &m : markers)
    {
        if(mlist.empty())
            estimatePose(m);
        else
        {
            for(auto &i : mlist)
            {
                if(m.id == i)
                {
                    estimatePose(m);
                    break;
                }
            }
        }
    }

    return ret;
}

std::map<int, Marker> ArucoPose::run(cv::Mat &left_image, cv::Mat &right_image, bool draw_markers)
{
    throw std::logic_error("ArucoPose stereo image not yet implemented");
}


bool ArucoPose::threePointsEstimation(std::map<int, Marker> markers, Pose &output)
{
    cv::Mat dummy;
    return threePointsEstimation(markers,output,dummy);
}

bool ArucoPose::threePointsEstimation(std::map<int, Marker> markers, Pose &output, cv::Mat &image_output)
{
    // For Three Points Estimation
    std::map<int, Marker> c_fi;

    uint8_t idx = 0;
    for(auto &i : mlist)
    {
        for(auto &m : markers)
        {
            if(i == m.first)
            {            
                c_fi[idx] = m.second;
                cv::transpose(c_fi[idx].Tvec,c_fi[idx].Tvec);
                cv::transpose(c_fi[idx].Rvec,c_fi[idx].Rvec);

                // cv::Mat m_rot = (cv::Mat_<float>(3,3) << 1,0,0,0,0,-1,0,1,0);
                // std::cout << "Tvec: " << c_fi[idx].Tvec << std::endl;
                // std::cout << "m_rot: " << m_rot << std::endl;
                // c_fi[idx].Tvec = m_rot * c_fi[idx].Tvec;
                // std::cout << "Res: " << c_fi[idx].Tvec << std::endl;                
                break;
            }
        }

        idx++;
    }

    if(c_fi.size() < 3) return false; // No necessary info to continue

    static std::vector<cv::Mat> c_rfi = {(cv::Mat_<double>(3,1) << 0,0,0), (cv::Mat_<double>(3,1) << 0,0,0),
             (cv::Mat_<double>(3,1) << 0,0,0)};
    static cv::Mat t_bc = (cv::Mat_<float>(4,4) << 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
    static cv::Mat t_fp = (cv::Mat_<float>(4,4) << 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);

    // Obtendo matrizes de rotacao
    // x
    c_rfi[0] = c_fi[1].Tvec - c_fi[0].Tvec;
    c_rfi[0] /= cv::norm(c_rfi[0], cv::NORM_INF);
    // z
    c_rfi[2] = c_rfi[0].cross(c_fi[2].Tvec - c_fi[0].Tvec);
    c_rfi[2] /= cv::norm(c_rfi[2], cv::NORM_INF);
    // y
    c_rfi[1] = c_rfi[2].cross(c_rfi[0]);

    // Rotation Matrix
    cv::Mat c_rf;
    cv::hconcat(c_rfi[0],c_rfi[1],c_rf);
    cv::hconcat(c_rf,c_rfi[2],c_rf);

    /*** Rotacionando a base! ***/
    // cv::Mat m_rot = (cv::Mat_<float>(3,3) << 1,0,0,0,0,-1,0,1,0);
    // c_rf = m_rot * c_rf;
    
    // Matriz de Transformacao Homogenea
    cv::Mat c_tf;
    // std::cout << "Rfc : " << c_rf << std::endl;
    // std::cout << "ofc : " << c_fi[0].Tvec << std::endl;
    cv::hconcat(c_rf, c_fi[0].Tvec, c_tf);
    cv::vconcat(c_tf, static_cast<cv::Mat>((cv::Mat_<float>(1,4) << 0,0,0,1)), c_tf);
    // std::cout << "Tfc : " << c_tf << std::endl;

    // Matriz de Transformacao Homogenea T_PB
    // c_tf = t_bc * c_tf * t_fp;
    
    // Posicao da Plataforma (Origem do frame P no frame B)
    output.pos = ( cv::Mat_<float>(3,1) << c_tf.at<float>(0,3), c_tf.at<float>(1,3), c_tf.at<float>(2,3) );

    // Orientacao da plataforma em relacao ao frame B (RPY) ---> em radianos!
    float b_rx = atan2(c_tf.at<float>(2,1), c_tf.at<float>(2,2)); // * 180/M_PI;
    float b_ry = atan2(-1 * c_tf.at<float>(2,0), sqrt(pow(c_tf.at<float>(2,1),2) + pow(c_tf.at<float>(2,2),2))); // * 180/M_PI;
    float b_rz = atan2(c_tf.at<float>(1,0), c_tf.at<float>(0,0)); // * 180/M_PI;

    output.rot = (cv::Mat_<float>(3,1) << b_rx, b_ry, b_rz);

    if(!image_output.empty())
        draw_axis(image_output, camParams.CameraMatrix, camParams.Distorsion,
                  output.rot, 
                  output.pos, c_fi[0].ssize, 6);

    return true;
}

void ArucoPose::drawMarker(cv::Mat &frame, Marker marker)
{
    marker.draw(frame, cv::Scalar(0,0,255), 4, false, false);
    CvDrawingUtils::draw3dCube(frame, marker, camParams, 2, yperpendicular);
    CvDrawingUtils::draw3dAxis(frame, marker, camParams, 6);
}

void ArucoPose::drawMarker(cv::Mat &frame, std::map<int, Marker> markers)
{
    for(auto &m : markers)
    {
        if(m.second.isPoseValid() && !m.second.Tvec.empty())
            drawMarker(frame, m.second);
    }
}
